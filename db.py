#!/usr/bin/env python
#
# File:         db.py
# Created:      171015
# Description:  db class
#

import os

class sqlite_db(object):
 '''sqlite3 wrapper'''

 def __init__(self, name = None, path = None):
  '''description'''

  import sqlite3 as s3

  if path is None:
   path = os.getcwd()

  if name is None:
   name = 'default.db'

  fullpath = path + '/' + name 

  try:
    self.db = s3.connect(fullpath)
  except:
    self.db = None

  if self.db is not None:
    self.cur = self.db.cursor()

 ####
 def sql(self, sql):
  self.cur.execute(sql)

 def close(self):
  self.db.close()


#####
'''Generic database class'''

class db(object):
 '''db class'''

 def __init__(self, name, path=None, dbtype='sqlite3'):
  self.db = sqlite_db(name, path) 

 def sql(self, sql):
  self.db.sql(sql)

## EOF ##
